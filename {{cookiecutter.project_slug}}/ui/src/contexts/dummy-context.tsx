import React from 'react';

const dummy_context = {
  value: "Empty"
}

export const DummyContext = React.createContext(
  dummy_context
);
import { IonContent, IonPage, IonChip, IonLabel } from '@ionic/react';
import React, { useState } from 'react';
import { Add } from '@material-ui/icons';
import { Button } from '@material-ui/core';

import AppBar from '../components/AppBar'

const Home: React.FC = () => {
  // Local states
  const [ title, setTitle ] = useState("Demo Title");
  const [ appMenuItems, setAppMenuItems ] = useState([
    {
      key: "1",
      label:"App Item 1",
      icon: (<Add/>),
      handler: () => { alert("Item 1")}
    },
    {
      key: "2",
      label:"App Item 2",
      handler: () => { alert("Item 2")}
    }
  ]);

  const [ chipMenuItems, setChipMenuItems ] = useState([
    {
      key: "1",
      label:"Settings...",
      handler: () => { alert("Settings")}
    },
    {
      key: "2",
      label:"Logout",
      handler: () => { alert("Logout")}
    }
  ]);

  return (
    <IonPage>
      <AppBar title={title} appMenuItems={appMenuItems} chipMenuItems={chipMenuItems} chipLabel="John D" />
      <IonContent className="ion-padding">
        <Button variant="contained" color="primary" startIcon={<Add/>}>Primary</Button>
        <IonChip>
          <IonLabel>Avatar Chip</IonLabel>
        </IonChip>
      </IonContent>
    </IonPage>
  );
};

export default Home;

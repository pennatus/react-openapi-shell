{% raw %}
import { IonHeader, IonTitle, IonToolbar, IonButtons, IonMenuButton, IonImg, IonChip, IonAvatar, IonLabel } from '@ionic/react';
import React from 'react';

import Menu from './Menu';
import Popover from './Popover';

import logo from '../assets/imgs/logo.png';
import { Face } from '@material-ui/icons';


interface IAppBar {
  title?: string;
  chipLabel?: string;
  appMenuItems?: {key: string, label: string, icon?: any, handler?: () => void}[];
  chipMenuItems?: {key: string, label: string, icon?: any, handler?: () => void}[];
}

const AppBar: React.FC<IAppBar> = ({title, chipLabel, appMenuItems=[], chipMenuItems=[]}) => {
  const appMenuId = "appMenuId";
  const showAppMenu = appMenuItems.length > 0;
  let chipJSX:any = ( <></> );

  if (chipLabel) {
    chipJSX = (
      <Popover items={chipMenuItems}>
        <IonChip>
          <IonAvatar>
            <Face/>
          </IonAvatar>
          <IonLabel>{chipLabel}</IonLabel>
        </IonChip>
      </Popover>
    )
  }

  return (
    <>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            { showAppMenu &&
              <IonMenuButton menu={appMenuId} />
            }
          </IonButtons>
          <div style={{display: "flex", flexDirection: "row"}}>
            <IonImg style={{width: "47px", height: "47px"}} src={logo} />
            <IonTitle>{title}</IonTitle>
            { chipJSX }
          </div>
        </IonToolbar>
      </IonHeader>
      <Menu header="App Menu" items={appMenuItems} menuId={appMenuId} side="start" />
    </>
  );
};

export default AppBar;
{% endraw %}

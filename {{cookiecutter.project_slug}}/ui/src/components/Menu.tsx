{% raw %}
import { IonMenu, IonHeader, IonToolbar, IonTitle, IonContent, IonButton } from '@ionic/react';
import React from 'react';

interface IMenu {
  menuId: string
  header?: string
  items?: {key: string, label: string, icon?: any, handler?: () => void}[]
  side?: "start" | "end" | undefined
}

const Menu: React.FC<IMenu> = ({menuId, header, items=[], ...props}) => {
  return (
    <IonMenu style={{height:"100vh"}} {...props} contentId={menuId} menuId={menuId}>
      {
        (header &&
          <IonHeader>
            <IonToolbar color="primary">
              <IonTitle>{header}</IonTitle>
            </IonToolbar>
          </IonHeader>
        )
      }
      <IonContent id={menuId}>
        <div style={{display:"flex", flexDirection:"column", alignItems:"start"}}>
          { items.map( (item) => {
            return (
              <IonButton key={item.key} fill="clear" onClick={item.handler}>
                {item.icon}
                <p>{item.label}</p>
              </IonButton>
            )
          })}
        </div>
      </IonContent>
    </IonMenu>
  );
};

export default Menu;
{% endraw %}

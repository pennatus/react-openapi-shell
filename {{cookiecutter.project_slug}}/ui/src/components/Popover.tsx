{% raw %}
import { IonPopover, IonContent, IonButton } from '@ionic/react';
import React, { useState } from 'react';

interface IPopover {
  children: any
  items?: {key: string, label: string, icon?: any, handler?: () => void}[]
}

interface IPopoverState {
  show: boolean
  event?: Event | undefined
}

const Popover: React.FC<IPopover> = ({children, items}) => {
  const [state, setState] = useState<IPopoverState>({show: false, event: undefined});

  return (
    <>
      <IonPopover
        isOpen={state.show}
        event={state.event}
        onDidDismiss={e => setState({show: false})}
      >
        <IonContent>
          <div style={{display:"flex", flexDirection:"column", alignItems:"start"}}>
            { items?.map( (item) => {
              return (
                <IonButton key={item.key} fill="clear" onClick={item.handler}>
                  {item.icon}
                  <p>{item.label}</p>
                </IonButton>
              )
            })}
          </div>
        </IonContent>
      </IonPopover>
      {
          React.cloneElement(children, { onClick: (e: any) => {
            e.persist();
            setState({show: true, event: e})
          } })
      }
    </>
  );
};

export default Popover;
{% endraw %}
